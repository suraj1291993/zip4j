# zip4j

Features over standart repo:

- maven support
- NativeFile / NativeStorage - java File class wrapper, which allows use unzip from InputStreams and native FileDiscriptors (useful on Android)
- Fixed broken windows default encdoings

# Exmples

Live example can be found in Torrent Client:

  * https://gitlab.com/axet/android-torrent-client/blob/master/app/src/main/java/com/github/axet/torrentclient/app/TorrentPlayer.java

# Unzip

```java
    File local = new File("test.zip");
    ZipFile zip = new ZipFile(new NativeStorage(local));
    List list = zip.getFileHeaders();
    for (Object o : list) {
        final FileHeader h = (FileHeader) o;
        System.println(h.isDirectory());
        System.println(h.getFileName());
        InputStream is = zip.getInputStream(zipEntry); // save to file
    }
```

# Central Maven Repo

```xml
    <dependency>
      <groupId>com.github.axet</groupId>
      <artifactId>zip4j</artifactId>
      <version>1.3.2-2</version>
    </dependency>
```

# Android Studio

```gradle
    compile ('com.github.axet:zip4j:1.3.2-2')
```