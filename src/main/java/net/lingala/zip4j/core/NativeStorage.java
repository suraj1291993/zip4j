package net.lingala.zip4j.core;

import java.io.File;
import java.io.FileNotFoundException;

import net.lingala.zip4j.util.InternalZipConstants;

public class NativeStorage {
    protected File f;

    public NativeStorage(NativeStorage v) {
        f = new File(f.getPath());
    }

    public NativeStorage(File f) {
        this.f = f;
    }

    public NativeFile read() throws FileNotFoundException {
        return new NativeFile(f, "r");
    }

    public NativeFile write() throws FileNotFoundException {
        return new NativeFile(f, "rw");
    }

    public NativeStorage open(String name) {
        return new NativeStorage(new File(f, name));
    }

    public boolean exists() {
        return f.exists();
    }

    public boolean canRead() {
        return f.canRead();
    }

    public boolean canWrite() {
        return f.canWrite();
    }

    public boolean isHidden() {
        return f.isHidden();
    }

    public NativeStorage getParent() {
        return new NativeStorage(f.getParentFile());
    }

    public String getName() {
        return f.getName();
    }

    public boolean isDirectory() {
        return f.isDirectory();
    }

    public long lastModified() {
        return f.lastModified();
    }

    public long length() {
        return f.length();
    }

    public boolean renameTo(NativeStorage f) {
        return this.f.renameTo(f.f);
    }

    public void setLastModified(long l) {
        f.setLastModified(l);
    }

    public void setReadOnly() {
        f.setReadOnly();
    }

    public boolean mkdirs() {
        return f.mkdirs();
    }

    public boolean delete() {
        return f.delete();
    }

    public NativeStorage[] listFiles() {
        File[] ff = f.listFiles();
        if (ff == null)
            return null;
        NativeStorage[] nn = new NativeStorage[ff.length];
        for (int i = 0; i < nn.length; i++) {
            nn[i] = new NativeStorage(ff[i]);
        }
        return nn;
    }

    public String getPath() {
        return f.getPath();
    }

    public String getRelPath(NativeStorage child) {
        String rootFolderFileRef = getPath();

        if (!rootFolderFileRef.endsWith(InternalZipConstants.FILE_SEPARATOR)) {
            rootFolderFileRef += InternalZipConstants.FILE_SEPARATOR;
        }

        String filePath = child.getPath();
        String tmpFileName = filePath.substring(rootFolderFileRef.length());
        if (tmpFileName.startsWith(System.getProperty("file.separator"))) {
            tmpFileName = tmpFileName.substring(1);
        }

        return tmpFileName;
    }

}
