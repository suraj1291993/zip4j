package net.lingala.zip4j.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class NativeFile {
    protected RandomAccessFile r;

    public NativeFile() {
    }

    public NativeFile(File f, String mode) throws FileNotFoundException {
        this.r = new RandomAccessFile(f, mode);
    }

    public long length() throws IOException {
        return r.length();
    }

    public void seek(long s) throws IOException {
        r.seek(s);
    }

    public void readFully(byte[] buf, int off, int len) throws IOException {
        r.readFully(buf, off, len);
    }

    public int read(byte[] buf) throws IOException {
        return r.read(buf);
    }

    public int read(byte[] buf, int off, int len) throws IOException {
        return r.read(buf, off, len);
    }

    public long getFilePointer() throws IOException {
        return r.getFilePointer();
    }

    public void close() throws IOException {
        r.close();
    }

    public void write(byte[] buf) throws IOException {
        r.write(buf);
    }

    public void write(byte[] b, int off, int len) throws IOException {
        r.write(b, off, len);
    }
}
